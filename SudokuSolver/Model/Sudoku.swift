//
//  Sudoku.swift
//  SudokuSolver
//
//  Created by Timotius Leonardo Lianoto on 01/03/21.
//

import Foundation

class Sudoku {
    var sudokuCollection = [ [ [3, 0, 6, 5, 0, 8, 4, 0, 0],
                               [5, 2, 0, 0, 0, 0, 0, 0, 0],
                               [0, 8, 7, 0, 0, 0, 0, 3, 1],
                               [0, 0, 3, 0, 1, 0, 0, 8, 0],
                               [9, 0, 0, 8, 6, 3, 0, 0, 5],
                               [0, 5, 0, 0, 9, 0, 6, 0, 0],
                               [1, 3, 0, 0, 0, 0, 2, 5, 0],
                               [0, 0, 0, 0, 0, 0, 0, 7, 4],
                               [0, 0, 5, 2, 0, 6, 3, 0, 0] ],
                             
                             [
                                [7, 8, 0, 4, 0, 0, 1, 2, 0],
                                [6, 0, 0, 0, 7, 5, 0, 0, 9],
                                [0, 0, 0, 6, 0, 1, 0, 7, 8],
                                [0, 0, 7, 0, 4, 0, 2, 6, 0],
                                [0, 0, 1, 0, 5, 0, 9, 3, 0],
                                [9, 0, 4, 0, 6, 0, 0, 0, 5],
                                [0, 7, 0, 3, 0, 0, 0, 1, 2],
                                [1, 2, 0, 0, 0, 7, 4, 0, 0],
                                [0, 4, 9, 2, 0, 6, 0, 0, 7]
                             ],
                             
                             [
                                [8,0,0,0,0,0,0,0,0],
                                [0,0,3,6,0,0,0,0,0],
                                [0,7,0,0,9,0,2,0,0],
                                [0,5,0,0,0,7,0,0,0],
                                [0,0,0,0,4,5,7,0,0],
                                [0,0,0,1,0,0,0,3,0],
                                [0,0,1,0,0,0,0,6,8],
                                [0,0,8,5,0,0,0,1,0],
                                [0,9,0,0,0,0,4,0,0]],
                             
                             [[5,1,7,6,0,0,0,3,4],
                              [2,8,9,0,0,4,0,0,0],
                              [3,4,6,2,0,5,0,9,0],
                              [6,0,2,0,0,0,0,1,0],
                              [0,3,8,0,0,6,0,4,7],
                              [0,0,0,0,0,0,0,0,0],
                              [0,9,0,0,0,0,0,7,8],
                              [7,0,3,4,0,0,5,6,0],
                              [0,0,0,0,0,0,0,0,0]],
                             [
                                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                [0, 0, 8, 0, 0, 0, 0, 4, 0],
                                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                [0, 0, 0, 0, 0, 6, 0, 0, 0],
                                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                [2, 0, 0, 0, 0, 0, 0, 0, 0],
                                [0, 0, 0, 0, 0, 0, 2, 0, 0],
                                [0, 0, 0, 0, 0, 0, 0, 0, 0]
                             ],
                             [[8, 1, 0, 0, 3, 0, 0, 2, 7],
                              [0, 6, 2, 0, 5, 0, 0, 9, 0],
                              [0, 7, 0, 0, 0, 0, 0, 0, 0],
                              [0, 9, 0, 6, 0, 0, 1, 0, 0],
                              [1, 0, 0, 0, 2, 0, 0, 0, 4],
                              [0, 0, 8, 0, 0, 5, 0, 7, 0],
                              [0, 0, 0, 0, 0, 0, 0, 8, 0],
                              [0, 2, 0, 0, 1, 0, 7, 5, 0],
                              [3, 8, 0, 0, 7, 0, 0, 4, 2]],
                             
                             [[5,1,7,6,0,0,0,3,4],
                              [2,8,9,0,0,4,0,0,0],
                              [3,4,6,2,0,5,0,9,0],
                              [6,0,2,0,0,0,0,1,0],
                              [0,3,8,0,0,6,0,4,7],
                              [0,0,0,0,0,0,0,0,0],
                              [0,9,0,0,0,0,0,7,8],
                              [7,0,3,4,0,0,5,6,0],
                              [0,0,0,0,0,0,0,0,0]],
                             
                             [[0, 4, 0, 7, 0, 0, 1, 3, 0],
                              [0, 0, 2, 0, 0, 0, 6, 0, 0],
                              [0, 0, 0, 4, 2, 0, 0, 0, 0],
                              [6, 0, 0, 0, 0, 2, 0, 0, 3],
                              [2, 3, 1, 0, 7, 0, 0, 8, 0],
                              [4, 0, 0, 3, 1, 0, 0, 0, 0],
                              [0, 7, 0, 0, 0, 8, 0, 0, 0],
                              [0, 0, 6, 0, 3, 0, 0, 0, 4],
                              [8, 9, 0, 0, 5, 0, 0, 0, 6]],
                             
                             [[0, 4, 2, 7, 8, 9, 1, 3, 5],
                              [0, 0, 2, 0, 0, 0, 6, 0, 0],
                              [0, 0, 0, 4, 2, 0, 0, 0, 0],
                              [6, 0, 0, 0, 0, 2, 0, 0, 3],
                              [2, 3, 1, 0, 7, 0, 0, 8, 0],
                              [4, 0, 0, 3, 1, 0, 0, 0, 0],
                              [0, 7, 0, 0, 0, 8, 0, 0, 0],
                              [0, 0, 6, 0, 3, 0, 0, 0, 4],
                              [8, 9, 0, 0, 5, 0, 0, 0, 6]],
                             
                             [
                                [3, 9, 0,   0, 5, 0,   0, 0, 0],
                                [0, 0, 0,   2, 0, 0,   0, 0, 5],
                                [0, 0, 0,   7, 1, 9,   0, 8, 0],
                                
                                [0, 5, 0,   0, 6, 8,   0, 0, 0],
                                [2, 0, 6,   0, 0, 3,   0, 0, 0],
                                [0, 0, 0,   0, 0, 0,   0, 0, 4],
                                
                                [5, 0, 0,   0, 0, 0,   0, 0, 0],
                                [6, 7, 0,   1, 0, 5,   0, 4, 0],
                                [1, 0, 9,   0, 0, 0,   2, 0, 0]
                             ]
                             
                            ]
    var currentRow: Int?
    var currentCol: Int?
    var sudokuSize = 9
    var sudokuEmpty = 0
    var sudokuTemp: [[Int]]?
    
    func chooseUnsolvedSudoku(puzzle: [[[Int]]]) -> [[Int]]{
        let randomInt = Int.random(in: 0..<sudokuCollection.count)
        return sudokuCollection[randomInt]
    }
    
    func copySelectedSudoku(puzzle: [[Int]]){
        sudokuTemp = [[Int]]()
        sudokuTemp = puzzle
    }
    
    func sudokuRowValidation(guess: Int, row: Int) -> Bool{
        for i in 0..<sudokuSize {
            if sudokuTemp![row][i] == guess {
                return true
            }
        }
        return false
    }
    
    func sudokuColValidation(guess: Int, col: Int) -> Bool{
        for i in 0..<sudokuSize {
            if sudokuTemp![i][col] == guess {
                return true
            }
        }
        return false
    }
    
    func sudokuBoxValidation(guess: Int, row: Int, col: Int) -> Bool{
        let r = row - row % 3
        let c = col - col % 3
        
        for row_ in r..<(r + 3) {
            for col_ in c..<(c + 3) {
                if sudokuTemp![row_][col_] == guess {
                    return true
                }
            }
        }
        return false
    }
    
    func sudokuGuessValidation(guess: Int, row: Int, col: Int) -> Bool{
        return !sudokuRowValidation(guess: guess, row: row) && !sudokuColValidation(guess: guess, col: col) && !sudokuBoxValidation(guess: guess, row: row, col: col)
    }
    
    func solveSudoku() -> Bool{
        for row in 0..<sudokuSize{
            for col in 0..<sudokuSize {
                if sudokuTemp![row][col] == sudokuEmpty {
                    for guess in 0...sudokuSize {
                        if sudokuGuessValidation(guess: guess, row: row, col: col) {
                            sudokuTemp![row][col] = guess
                            
                            if solveSudoku(){
                                return true
                            }else {
                                sudokuTemp![row][col] = sudokuEmpty
                            }
                        }
                    }
                    
                    return false
                }
            }
        }
        
        return true
    }
}
