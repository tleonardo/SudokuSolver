//
//  ViewController.swift
//  SudokuSolver
//
//  Created by Timotius Leonardo Lianoto on 28/02/21.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - Global Variable
    var sudoku = Sudoku()
    var sudokuUnsolved = [[Int]]()
    var sudokuUnsolved2 = [[Int]]()
    var time: TimeInterval = 1800
    var timer = Timer()
    var isPause = false
    var isUserInteractionOnBoard = false
    var inputNumberRow = 0
    var inputNumberCol = 0
    var emptySquareIndex = [Int]()
    var emptySquareInputAllow = true
    private var boardCellIdentifier = "boardCell"
    private var numberInputCellIdentifier = "numberInputCell"
    
    // MARK: - IBOutlet
    @IBOutlet var newGameButton: UIButton!
    @IBOutlet var pauseButton: UIButton!
    @IBOutlet var autoSolveButton: UIButton!
    @IBOutlet var staticBackground: UIView!
    @IBOutlet var boardCollectionView: UICollectionView!
    @IBOutlet var numberInputCollectionView: UICollectionView!
    @IBOutlet var timerLabel: UILabel!
    
    // MARK: - App Flow Function
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        startNewGame()
    }
    
    // MARK: - Universal Function
    func startNewGame() {
        timer.invalidate()
        self.sudokuUnsolved = sudoku.chooseUnsolvedSudoku(puzzle: sudoku.sudokuCollection)
        sudoku.copySelectedSudoku(puzzle: self.sudokuUnsolved)
        self.time = 1800
        timerLabel.text = timerSetUp(timer: self.time)
        runTimer()
        showToast(controller: self, message: "New Game Start!", seconds: 1.5)
    }
    
    private func setUpView(){
        newGameButton.backgroundColor = UIColor(named: "AccentColor")
        autoSolveButton.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        autoSolveButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        staticBackground.backgroundColor = UIColor(named: "SecondColor")
        boardCollectionView.backgroundColor = UIColor(white: 0, alpha: 0)
        boardCollectionView.delegate = self
        boardCollectionView.dataSource = self
        numberInputCollectionView.dataSource = self
        numberInputCollectionView.delegate = self
        numberInputCollectionView.alpha = 0
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimerLabel), userInfo: nil, repeats: true)
    }
    
    func timerSetUp(timer: TimeInterval) -> String{
        let hours = Int(timer) / 3600
        let minutes = Int(timer) / 60 % 60
        let seconds = Int(timer) % 60
        return String(format: "%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    @objc func updateTimerLabel(){
        if self.time > 0 {
            self.time -= 1
            self.timerLabel.text = timerSetUp(timer: self.time)
        }else {
            self.timer.invalidate()
            let alert = UIAlertController(title: "GAME OVER", message: "Do you want to play again?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { choice in
                self.sudokuUnsolved = [[Int]]()
                self.sudoku = Sudoku()
                self.startNewGame()
                self.boardCollectionView.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { choice in
                // nothing to do
            }))
            
        }
    }
    
    func showToast(controller: UIViewController, message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.white
        alert.view.alpha = 0.8
        alert.view.layer.cornerRadius = 15
        
        controller.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true)
        }
    }
    
    func changeTwoArrayIntoOne(puzzle: [[Int]]) -> [Int]{
        var tempArray = [Int]()
        for i in 0..<sudoku.sudokuSize {
            for j in 0..<sudoku.sudokuSize {
                tempArray.append(puzzle[i][j])
            }
        }
        return tempArray
    }
    
    func solvingSudoku() {
        if sudoku.solveSudoku() {
            for row in 0..<sudoku.sudokuSize {
                for col in 0..<sudoku.sudokuSize {
                    sudokuUnsolved[row][col] = sudoku.sudokuTemp![row][col]
                }
            }
        }else {
            showToast(controller: self, message: "This Sudoku is Unsolveable", seconds: 2)
        }
    }
    
    func endGameChecker(){
        var isDone = true
        for item in 0..<sudoku.sudokuSize {
            if self.sudokuUnsolved2[item].contains(0) {
                isDone = false
            }
        }
        if isDone == true {
            if sudokuUnsolved2 == sudokuUnsolved {
                showToast(controller: self, message: "You Have Guess it Perfectly, Well Done!", seconds: 3)
            }else {
                showToast(controller: self, message: "You still have the wrong guess, please try again...", seconds: 3)
            }
        }
    }
    
    // MARK: - IBAction
    @IBAction func newGameButtonPressed(_ sender: Any) {
        self.boardCollectionView.isUserInteractionEnabled = true
        sudokuUnsolved = [[Int]]()
        sudoku = Sudoku()
        startNewGame()
        boardCollectionView.reloadData()
    }
    
    @IBAction func pauseButtonPressed(_ sender: Any) {
        if self.isPause == true {
            self.boardCollectionView.isUserInteractionEnabled = true
            showToast(controller: self, message: "Game Unpaused", seconds: 2)
            runTimer()
            self.isPause = false
            pauseButton.setImage(UIImage(systemName: "pause.fill"), for: .normal)
        }else {
            self.timer.invalidate()
            self.boardCollectionView.isUserInteractionEnabled = false
            showToast(controller: self, message: "Game Paused", seconds: 2)
            self.isPause = true
            pauseButton.setImage(UIImage(systemName: "play.fill"), for: .normal)
        }
    }
    @IBAction func solveButtonPressed(_ sender: Any) {
        solvingSudoku()
        self.isUserInteractionOnBoard = false
        self.boardCollectionView.reloadData()
        self.boardCollectionView.isUserInteractionEnabled = false
        showToast(controller: self, message: "Sudoku Auto-Solved!", seconds: 1.5)
    }
    
}

// MARK:  - Collection View Data Source
extension ViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == boardCollectionView {
            return 1
        }else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == boardCollectionView {
            return sudoku.sudokuSize*sudoku.sudokuSize
        }else {
            return 9
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == boardCollectionView {
            var arrayToInput = [Int]()
            if self.isUserInteractionOnBoard == false {
                arrayToInput = changeTwoArrayIntoOne(puzzle: sudokuUnsolved)
            }else {
                arrayToInput = changeTwoArrayIntoOne(puzzle: sudokuUnsolved2)
            }
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: boardCellIdentifier, for: indexPath) as? BoardCollectionViewCell else {
                fatalError("Cant Find BoardCollectionViewCell")
            }
            cell.numberLabel.textColor = UIColor.white
            if String(arrayToInput[indexPath.row]) != "0" {
                cell.numberLabel.text = String(arrayToInput[indexPath.row])
            }else {
                cell.numberLabel.text = ""
                if self.emptySquareInputAllow == true {
                    self.emptySquareIndex.append(indexPath.row)
                }
            }
            
            return cell
        }else {
            let arrayToInput = [1,2,3,4,5,6,7,8,9]
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: numberInputCellIdentifier, for: indexPath) as? NumberInputCollectionViewCell else {
                fatalError("Cant Find NumberInputCollectionViewCell")
            }
            cell.numberLabel.textColor = UIColor.black
            cell.numberLabel.text = String(arrayToInput[indexPath.row])
            return cell
        }
    }
    
}

// MARK:  - Collection View Delegate
extension ViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == boardCollectionView {
            self.emptySquareInputAllow = false
//            guard let cell = collectionView.cellForItem(at: indexPath) as? BoardCollectionViewCell else {fatalError("No BoardCell")}
            if emptySquareIndex.contains(indexPath.row){
                if self.isUserInteractionOnBoard == false {
                    sudokuUnsolved2 = sudokuUnsolved
                    self.isUserInteractionOnBoard = true
                    solvingSudoku()
                }
                self.inputNumberRow = indexPath.row + 1
                self.inputNumberCol = indexPath.row + 1
                showToast(controller: self, message: "Pick 1 from list below", seconds: 1)
                boardCollectionView.isUserInteractionEnabled = false
                UIView.animate(withDuration: 0.5) {
                    self.numberInputCollectionView.alpha = 1
                }
                self.boardCollectionView.reloadData()
            }
        }else {
            guard let cell = collectionView.cellForItem(at: indexPath) as? NumberInputCollectionViewCell else {fatalError("No NumberInputCell")}
            var column = Int()
            var row = Int()
            // mencari nilai column dari indexpath.row
            while self.inputNumberCol > 9 {
                self.inputNumberCol -= 9
            }
            column = self.inputNumberCol - 1
            
            // mencari nilai row dari indexpath.row
            for i in 0...9 {
                if (9 * i) >= self.inputNumberRow {
                    row = i - 1
                    break
                }
            }
            
            // assign value ke sudokuUnsolved2
            sudokuUnsolved2[row][column] = Int(cell.numberLabel.text ?? "0") ?? 0
            self.boardCollectionView.reloadData()
            endGameChecker()
            UIView.animate(withDuration: 0.5) {
                self.numberInputCollectionView.alpha = 0
            }
            self.boardCollectionView.isUserInteractionEnabled = true
        }
    }
}

// MARK:  - Collection View Flow Layout
extension ViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == boardCollectionView {
            return CGSize(width: (collectionView.bounds.width/9)-1 , height: (collectionView.bounds.width/9)-0.5)
        }else {
            return CGSize(width: (collectionView.bounds.width/9)-1, height: (collectionView.bounds.height/9)-1)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == boardCollectionView {
            return 0
        }else {
            return 14
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == boardCollectionView {
            return 0
        }else {
            return 14
        }
    }
}

